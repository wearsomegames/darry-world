﻿using System;

namespace DarryWorld
{
	public class Stone : Tile
	{
		public Stone ()
		{
		}

		public override Tile CreateNew ()
		{
			Tile stone = new Stone ();
			stone.texture = TextureManager.LoadSolidBlock("Stone");
			return stone;
		}
	}
}

