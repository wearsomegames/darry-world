﻿using System;

namespace DarryWorld
{
	public class Grass : Tile
	{
		public Grass ()
		{
			
		}

		public override Tile CreateNew ()
		{
			Tile grass = new Grass ();
			grass.texture = TextureManager.LoadSolidBlock("Grass");
			return grass;
		}
	}
}

