﻿using System;

//A very simple generator lacking biomes, it creates a world with slopes going from 1 to 29,000 back to 1.
//      ---      ---
//   ---  |   ---  |
//---     |---     | ...

namespace DarryWorld
{
	public class Slopeworld : IGenerator
	{
		private int seed=1111;
		private World world;
		private SuperChunk superRef;
		private Chunk chunkRef;
		private LayerChunk layerRef;
		public Slopeworld ()
		{
		}

		public World InvokeGenerator ()
		{
			return world = new World(this);
		}

		public void GenerateSuper (WorldPosition pos)
		{
			superRef = new SuperChunk(world, pos);
			world.AllocateSuper(pos, superRef);
		}

		public void GenerateChunk (WorldPosition pos)
		{
			superRef = world.GetSuper(pos);
			chunkRef = new Chunk(pos.ToSuper, superRef, world);
			superRef.AllocateChunk(chunkRef.Position, chunkRef);
		}

		public void GenerateLayer (WorldPosition pos)
		{
			superRef = world.GetSuper (pos);
			chunkRef = superRef.GetChunk (pos.ToSuper);
			layerRef = new LayerChunk (pos.ToSuper.ToChunk, chunkRef, world);
			if (layerRef.Position.Y - 15 <= 0) 
			{
				layerRef.FillLayerByRegion(TileManager.GetNewTile("Grass"), 
					new LayerPosition(0,0,null),
					new LayerPosition(16,(int)(16-layerRef.Position.Y),null));
			}
			chunkRef.AllocateLayer(layerRef.Position, layerRef);
		}

		public void GenerateWithinRadius(int radius, WorldPosition origin)
		{

		}
	}
}

