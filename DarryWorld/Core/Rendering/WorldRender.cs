﻿using System;
using System.Collections.Generic;
using OpenTK.Graphics.OpenGL;
using System.Drawing;
using OpenTK;

namespace DarryWorld
{

	public class WorldRender
	{
		//General view will be at standert zoom, 2.2
		private static WorldPosition lastWPos;
		private static List<Tile[,]> toSend;
		private static int[,] coverMap;
		private static int tilesCovered;

		public static void RenderView (WorldPosition wPos, Camera cam, SuperChunk sChunk)
		{
			//Console.WriteLine ("Hit Render");
			lastWPos = wPos;
			coverMap = new int[16, 16];
			tilesCovered = 0;
			Chunk refChunk = sChunk.GetChunk (wPos.ToSuper);
			LayerChunk refLayer = refChunk.GetLayerChunk (wPos.ToSuper.ToChunk);
			for (int i = (int)cam.Position.Y; i >= 0 && tilesCovered != 256; i--) {
				if (i == 0) {
					//Console.WriteLine("Bottom");
				}
				RecordLayer (refLayer);
				if (refLayer.AccompaningLowerRef != null)
					refLayer = refLayer.AccompaningLowerRef;
				else if (refLayer.ParentRef.AccompaningLowerRef != null) {
					refChunk = refLayer.ParentRef.AccompaningLowerRef;
					refLayer = refChunk.GetLayerChunk (new ChunkPosition (15, refChunk.Position));
				}
				//Console.WriteLine ("interation:{0}, tilesCovered:{1}", i, tilesCovered);
			}
			for (int i = refLayer.WorldY; i < cam.Position.Y + 1; i++) {
				refLayer.ChangeTileScales (new Vector2 ((float)FindTileSize (cam, wPos), (float)FindTileSize (cam, wPos)));
				RenderLayer(refLayer, cam);
				if (refLayer.AccompaningUpperRef == null)
					refLayer = refLayer.ParentRef.GetLayerChunk(new ChunkPosition(0, wPos.ToSuper));
				else
					refLayer = refLayer.AccompaningUpperRef;
			}
		}

		private static void RecordLayer (LayerChunk layer)
		{
			Tile[,] tiles = layer.GetTiles ();
			for (int i = 0; i < tiles.Length/16; i++) 
			{
				for (int j = 0; j < tiles.Length/16; j++) 
				{
					if (coverMap [i, j] == 0 && tiles [i, j] != null) 
					{
						coverMap[i,j]=1;
						tilesCovered++;
					}
				}
			}
		}

		public static void RenderLayer (LayerChunk layer, Camera cam)
		{
			Tile[,] tiles = layer.GetTiles ();
			ChunkPosition lpos = layer.Position;
			Vector3 pos;
			for (int i = 0; i < 16; i++) 
			{
				for (int j = 0; j < 16; j++) 
				{
					if (tiles [i, j] != null) 
					{
						lpos.ToLayer = new LayerPosition(j,i, layer.Position);
						pos = layer.TilePosition(i,j);
						//Vector2 scale = new Vector2((float)FindTileSize(cam, layer.Position.ToSuper.ToWorld),(float)FindTileSize(cam, layer.Position.ToSuper.ToWorld));
						Mapper.MapTexture(tiles[i,j].Texture, new Vector2(pos.X - cam.Position.X + (15 * lpos.ToSuper.ToWorld.X), pos.Z - cam.Position.Z + (15 * lpos.ToSuper.ToWorld.Z)), Vector2.Zero, tiles[i,j].Scale, Color.Transparent);
					}
				}
			}
		}

		private static double FindTileSize (Camera camPos, WorldPosition tilePos)
		{
			float mod = (camPos.Position.Y-tilePos.ToSuper.Y) % (int)(Math.PI/2);
			float acos = (float)Math.Acos(mod);
			float sin = (float)Math.Sin(acos)+1;
			float val = ((float)((16 * (tilePos.ToSuper.Y)) + (tilePos.ToSuper.ToChunk.Y)+1))/(sin * (camPos.Position.Y-tilePos.ToSuper.Y));
			return val;
		}
	}
}