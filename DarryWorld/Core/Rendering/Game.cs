﻿using System;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using System.Drawing;
using System.Collections.Generic;
using System.Threading;

namespace DarryWorld
{
	public class Game : GameWindow
	{
		World world;
		IGenerator generator = new Slopeworld();
		Camera camera = new Camera(new Vector3(0,16f,0),2, 0);
		private WorldPosition iwPos;

		public Game (int width, int height)
			:base(width, height)
		{
			GL.Enable (EnableCap.Texture2D);
			Console.WriteLine ("Game Window instantiating...");
			GameInput.Initalize (this);
			TextureManager.Initalize();
			Console.WriteLine ("Game Window instantiated!");
		}

		protected override void OnLoad (EventArgs e)
		{
			Console.WriteLine ("Loading");
			base.OnLoad (e);
			world = generator.InvokeGenerator();
		}

		protected override void OnUpdateFrame (FrameEventArgs e)
		{
			base.OnUpdateFrame (e);
			GameInput.Update();
			camera.Update();
		}

		protected override void OnRenderFrame (FrameEventArgs e)
		{
			base.OnRenderFrame (e);

			GL.Clear (ClearBufferMask.ColorBufferBit);
			GL.ClearColor (Color.DarkSlateGray);

			Mapper.Begin (this.Width, this.Height);
			camera.ApplyTransform ();

			List<WorldPosition> wPositions = new List<WorldPosition> ();
			double mod = (double)((camera.Position.Y) % (int)(Math.PI/2));
			double acos = Math.Acos(mod);
			double sin = Math.Sin(acos);
			double tpos = sin * ((double)camera.Position.Y+1);
			for (int i = -((int)tpos+1); i <= (int)tpos+1; i++) 
			{
				for (int j = -((int)tpos+1); j <= (int)tpos+1; j++) 
				{
					if (tpos*2 >= Math.Sqrt((double)((i*i)+(j*j)))) 
					{
						wPositions.Add(new WorldPosition(new Vector3(16 * (camera.Position.X+i), camera.Position.Y, 16 * (camera.Position.Z+j))));
					}
				}
			}
			for (int i=0;i<wPositions.Count;i++)
				WorldRender.RenderView (wPositions[i], camera, world.GetSuper (wPositions[i]));

			this.SwapBuffers ();
		}
	}
}

