﻿using System;

using System.Drawing;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace DarryWorld
{
	public class Mapper
	{
		public static void MapTexture(
			Texture2D texture, 
			Vector2 origin, 
			Vector2 position, 
			Vector2 scale, 
			Color color)
		{
			Vector2[] vertices= new Vector2[4]
			{
				new Vector2(0,0),
				new Vector2(1,0),
				new Vector2(1,1),
				new Vector2(0,1)
			};
			GL.BindTexture (TextureTarget.Texture2D, texture.ID);

			GL.Begin (PrimitiveType.Quads);

			GL.Color3 (color);

			for (int i = 0; i < vertices.Length; i++) 
			{
				GL.TexCoord2(vertices[i]);

				vertices[i] += position;
				vertices[i] -= origin;
				vertices[i].X *= texture.Width;
				vertices[i].Y *= texture.Height;
				vertices[i] *= scale;

				GL.Vertex2(vertices[i]);
			}

			GL.End ();
			GC.Collect();
		}

		public static void Begin(int screenWidth, int screenHeight)
		{
			GL.MatrixMode (MatrixMode.Projection);
			GL.LoadIdentity ();

			GL.Ortho (-screenWidth / 2f, screenWidth / 2f, screenHeight / 2f, -screenWidth / 2f, 0f, 1f);
		}
	}
}

