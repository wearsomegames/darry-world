﻿using System;
using OpenTK;
using OpenTK.Graphics.OpenGL;

//zoom 2.56 : 3x3 room
//zoom > 0 (prefer min 0.2)

namespace DarryWorld
{
	public enum TweenType 
	{
		Instant,
		Linear,
		QuadraticInOut,
		CubicInOut,
		QuarticOut
	}

	public class Camera
	{
		private Vector3 position;
		private double rotation;
		private double zoom;
		private Vector3 positionGoto, positionFrom;
		private TweenType tweenType;
		private int currentStep, tweenStep;

		public Vector3 Position 
		{
			get 
			{
				return position;
			}
		}
		public Vector3 PositionGoto
		{
			get 
			{
				return positionGoto;
			}
		}

		public Camera (Vector3 startPos, double startZoom = 1.0, double startRotation = 0.0)
		{
			this.position = startPos;
			this.positionGoto = startPos;
			this.zoom = startZoom;
			this.rotation = startRotation;
		}

//		public Vector3 ToWorld(Vector3 input)
//		{
//			input /= (float)zoom;
//			Vector3 dx = new Vector3 ((float)Math.Cos (rotation), (float)Math.Sin (rotation));
//			Vector3 dy = new Vector3 ((float)Math.Cos (rotation + MathHelper.PiOver2), (float)Math.Sin (rotation + MathHelper.PiOver2));
//
//			return (this.position + (dx * input.X) + (dy * input.Y));
//		}

		public void Update()
		{
			if (currentStep < tweenStep) 
			{
				currentStep++;

				switch (tweenType) 
				{
					case TweenType.Linear:
						position = positionFrom + (positionGoto - positionFrom) * GetLinear ((float)
							currentStep / tweenStep);
						break;
					case TweenType.QuadraticInOut:
						position = positionFrom + (positionGoto - positionFrom) * GetQuadraticInOut ((float)
							currentStep / tweenStep);
						break;
					case TweenType.CubicInOut:
						position = positionFrom + (positionGoto - positionFrom) * GetCubicInOut ((float)
							currentStep / tweenStep);
						break;
					case TweenType.QuarticOut:
						position = positionFrom + (positionGoto - positionFrom) * GetQuadraticOut ((float)
							currentStep / tweenStep);
						break;
				}
			} 
			else 
			{
				position = positionGoto;
			}
		}

		public void SetPosition(Vector3 newPosition)
		{
			this.positionFrom = newPosition;
			this.position = newPosition;
			this.positionGoto = newPosition;
			tweenType = TweenType.Instant;
			currentStep = 0;
			tweenStep = 0;
		}

		public void SetPosition(Vector3 newPosition, TweenType type, int numSteps)
		{
			this.positionFrom = position;
			this.position = newPosition;
			this.positionGoto = newPosition;
			tweenType = type;
			currentStep = 0;
			tweenStep = numSteps;
		}

		public float GetLinear(float t)
		{
			return t;
		}

		public float GetQuadraticInOut(float t)
		{
			return (t * t) / ((2 * t * t) - (2 * t) + 1);
		}

		public float GetCubicInOut(float t)
		{
			return (t * t * t) / ((3 * t * t) - (3 * t) + 1);
		}

		public float GetQuadraticOut(float t)
		{
			return -((t-1) * (t-1) * (t-1) * (t-1)) + 1;
		}

		public void ApplyTransform()
		{
			Matrix4 transform = Matrix4.Identity;

			transform = Matrix4.Mult (transform, Matrix4.CreateTranslation(-position.X, -position.Y, 0));
			transform = Matrix4.Mult (transform, Matrix4.CreateRotationZ(-(float)rotation));
			transform = Matrix4.Mult(transform, Matrix4.CreateScale((float)zoom, (float)zoom, 1.0f));

			GL.MultMatrix(ref transform);
		}
	}
}

