﻿using System;
using System.Collections.Generic;

namespace DarryWorld
{
	public static class GeneratorManager
	{
		//TODO: Implement
		//hold all implemented generators.

		private static Dictionary<string, IGenerator> generators = new Dictionary<string, IGenerator>();

		public static void AddGenerator(string name, IGenerator generator)
		{
			generators.Add(name, generator);
		} 

		public static World InvokeGenerator(string name, int seed)
		{
			return generators [name].InvokeGenerator();
		}
	}
}