﻿using System;

namespace DarryWorld
{
	public interface IGenerator
	{
		World InvokeGenerator();
		void GenerateSuper(WorldPosition pos);
		void GenerateChunk(WorldPosition pos);
		void GenerateLayer(WorldPosition pos);
		void GenerateWithinRadius(int radius, WorldPosition origin);
	}
}

