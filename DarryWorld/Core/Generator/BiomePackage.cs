﻿using System;
using System.Collections.Generic;

//Chunk package is used for generation of biomes.
//When a whole biome is created, it is stretched out
//into many chunks. BiomePackage will help generators
//create biomes without writing over existing chunks
//by setting the region which non-allocated superchunks
//can be used to create the biome.

namespace DarryWorld
{
	public class BiomePackage
	{
		private World worldRef;
		private List<WorldPosition> allocatedCoords = new List<WorldPosition> ();

		public BiomePackage (World world, WorldPosition pos, double direction)
		{
			worldRef = world;
			AllocateSuperChunkPositions (pos, direction);
		}

		private void AllocateSuperChunkPositions(WorldPosition pos, double direction)
		{
			//Try to allocate a circular area for which chunks can be rendered given point.
			WorldPosition newPos = new WorldPosition
				(
					pos.X + 96 * (int)Math.Cos(direction), //96 = 1.5 of the biome radi, radi=64
					pos.Z + 96 * (int)Math.Sin(direction)
				);
			WorldPosition allocPos;
			for (int i = -32; i < 33; i++) 
			{
				for (int j = -32; j < 33; j++)
				{
					allocPos = new WorldPosition(newPos.X + i, newPos.Z + j);
					//   within radius                                       //check if already allocated
					if (((i+32) * (i+32)) + ((j+32) * (j + 32)) <= 64 * 64 && !worldRef.IsAllocated(allocPos))
					{
						allocatedCoords.Add(allocPos);
					}
				}
			}//TODO: Add a random curve over shoot for generation, in other words, make generation not a circle.
		}
	}
}