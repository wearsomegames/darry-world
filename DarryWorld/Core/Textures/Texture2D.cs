﻿using System;
using OpenTK;

namespace DarryWorld
{
	public struct Texture2D
	{
		private int id;
		private int width, height;

		public int ID
		{
			get{ return id;}
		}
		public int Width
		{
			get{ return width;}
		}
		public int Height
		{
			get{ return height;}
		}

		public Texture2D(int id, int width=32, int height=32)
		{
			this.id = id;
			this.width = width;
			this.height = height;
		}
	}
}

