﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Reflection;
using OpenTK.Graphics.OpenGL;
using System.Drawing;
using System.Drawing.Imaging;

namespace DarryWorld
{
	public static class TextureManager
	{
		private static Dictionary<string, Texture2D> textures = new Dictionary<string, Texture2D>();
		private static int id;
		///home/darius/Programming/CSharp/DarryWorld/DarryWorld/bin/Debug/Implemented/Graphics/Tiles/Solid
		private static string home = AppDomain.CurrentDomain.BaseDirectory;
		private static string graphics = "Implemented/Graphics";
		private static string tiles = graphics + "/Tiles";
		private static string tiles_Solid = tiles + "/Solid";
		private static string tiles_Transparent = tiles + "/Transparent";
		private static string entities = "/Entities";

		public static void Initalize()
		{
			Console.WriteLine ("Loading textures...");
			TileManager.Initalize();
			Console.WriteLine ("Textures Loaded!");
		}

		public static Texture2D LoadSolidBlock (string name)
		{
			Texture2D tex = LoadTexture(tiles_Solid + "/" + name + ".jpg", name);
			return tex;
		}

//		public static Texture2D LoadTransBlock (string name)
//		{
//			
//		}
//
//		public static Texture2D LoadEntityTexture ()
//		{
//			
//		}

		private static Texture2D LoadTexture (string path, string name)
		{
			if (!File.Exists (path)) 
			{
				throw new FileNotFoundException ("File not found at '" + path + "'");
			}

			int id = GL.GenTexture();
			GL.BindTexture(TextureTarget.Texture2D, id);

			Bitmap bmp = new Bitmap(path);
			BitmapData data = bmp.LockBits(new Rectangle(0,0,bmp.Width,bmp.Height), ImageLockMode.ReadOnly,
				System.Drawing.Imaging.PixelFormat.Format32bppArgb);

			GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, data.Width, data.Height, 0, 
				OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, data.Scan0);

			bmp.UnlockBits(data);

			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.Clamp);
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.Clamp);
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
			GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);

			return new Texture2D(id, bmp.Width, bmp.Height);
		}
	}
}

