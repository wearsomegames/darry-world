﻿using System;
using System.Collections.Generic;

namespace DarryWorld
{
	public class Cmdlet
	{
		protected internal string cmdletName;
		public string CmdletName 
		{
			get 
			{
				return cmdletName;
			}
		}

		public virtual void InvokeCmdlet (List<string> args)
		{
		}
	}
}

