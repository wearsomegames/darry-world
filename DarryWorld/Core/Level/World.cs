﻿using System;
using System.Collections.Generic;

namespace DarryWorld
{
	public class World
	{//2147483647
		public readonly int WORLD_RADIUS = (int)Math.Sqrt(2 * Math.Pow(int.MaxValue, 2));
		private IGenerator worldGenerator;
		private List<List<WorldPosition>> worldPosFinder;
		private Dictionary<WorldPosition, SuperChunk> superChunks = new Dictionary<WorldPosition, SuperChunk> ();
		private List<Entity> entities = new List<Entity>(); //all entities within range

		public IGenerator Generator 
		{
			get
			{
				return worldGenerator;
			}
		}

		public World (IGenerator generator)
		{
			worldGenerator = generator;
			Console.WriteLine ("World instantiated");
			//TODO : Implement
			//Add a update function which Game.Update() will call.
			//	within update :
			//		Invoke AI
			//		update chunk
			//		pick up items
			//		return textures to draw.
		}

		public void AllocateSuper(WorldPosition pos, SuperChunk chunk)
		{
			superChunks.Add (pos, chunk);
		}

		public bool IsAllocated(WorldPosition pos)
		{
			return superChunks.ContainsKey (pos);	
		}

		public WorldPosition FindMatchingPosition (WorldPosition pos)
		{
			return worldPosFinder[pos.X][pos.Z];
		}

//		public Dictionary<WorldPosition, SuperChunk> GetChunksByRadius(int radius, WorldPosition pos)
//		{
//			Dictionary<WorldPosition, SuperChunk> chunkShipment = new Dictionary<WorldPosition, SuperChunk>();
//			WorldPosition posi;
//			for (int i = -radius; i < radius + 1; i++)
//			{
//				for (int j= -radius;j < radius + 1; j++)
//				{
//					posi = new WorldPosition (pos.X + i, pos.Z + j);
//					if (!superChunks.ContainsKey (posi))
//						worldGenerator.GenerateWithinRadius (radius, pos);
//					if (MathUtil.WithinRadius (radius, pos))
//						chunkShipment.Add (superChunks[pos]);
//				}
//			}
//			return chunkShipment;
//		}
		public SuperChunk GetSuper (WorldPosition pos)
		{
			if (!ContainsPosition(pos))
				worldGenerator.GenerateSuper(pos);
			return GetByPosition(pos);
		}

		private bool ContainsPosition(WorldPosition wPos)
		{
			foreach(WorldPosition cWPos in superChunks.Keys)
			{
				if (wPos.X == cWPos.X && wPos.Z == cWPos.Z)
				{
					return true;
				}
			}
			return false;
		}

		private SuperChunk GetByPosition (WorldPosition wPos)
		{
			foreach (WorldPosition cWPos in superChunks.Keys) 
			{
				if (wPos.X == cWPos.X && wPos.Z == cWPos.Z)
				{
					return superChunks[cWPos];
				}
			}
			return null;
		}
	}
}

