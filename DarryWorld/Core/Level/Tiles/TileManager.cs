﻿using System;
using System.Collections.Generic;

//Tile manager is also a sprite batch for tiles.

namespace DarryWorld
{
	public static class TileManager
	{
		private static Dictionary<string, Tile> templateTiles = new Dictionary<string, Tile>();
		internal static bool loaded = false; //Can't add more stuff

		public static void Initalize()
		{
			AddTemplate("Grass", new Grass());
			AddTemplate("Stone", new Stone());
		}

		private static void AddTemplate(string name, Tile tile)
		{
			if (!loaded) 
			{
				templateTiles.Add (name, tile);	
			} 
			else 
			{
				//throw alreadyloadedexception
			}
		}

		public static Tile GetNewTile(string name)
		{
			return templateTiles [name].CreateNew();
		}
	}
}