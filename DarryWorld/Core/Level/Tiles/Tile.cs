﻿using System;
using OpenTK.Graphics.OpenGL;
using OpenTK;

namespace DarryWorld
{
	public class Tile : IDisposable
	{
		protected internal Texture2D texture;
		protected internal EventHandler DisposeTile;
		protected internal Vector2 postion;
		protected internal Vector2 scale;

		public Texture2D Texture 
		{
			get 
			{ 
				return texture; 
			} 
		}

		public Vector2 Position 
		{
			get
			{
				return postion;
			}
		}

		public Vector2 Scale 
		{
			get 
			{
				return scale;
			}
			set
			{
				scale = value;
			}
		}

		public Tile ()
		{
			//TODO: Implement
		}

		public virtual Tile CreateNew()
		{
			return new Tile();
		}

		public void Dispose()
		{
			DisposeTile.Invoke(this, null);
			GL.DeleteTexture(texture.ID);
		}
	}
}

