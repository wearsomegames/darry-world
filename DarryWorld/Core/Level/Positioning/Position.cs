﻿using System;

//Used by entities only! Used to find position in entire world.

namespace DarryWorld
{
	public struct Position
	{
		private double x;
		private double y;
		private double z;
		public double X
		{
			get 
			{
				return x;
			}
			set 
			{
				if (value < Int32.MinValue || value > Int32.MaxValue) {
					throw new Exception ("Out of range!");
				} else {
					x = value;
				}
			}
		}
		public double Y
		{
			get 
			{
				return x;
			}
			set 
			{
				if (value < Int32.MinValue || value > Int32.MaxValue) {
					throw new Exception ("Out of range!");
				} else {
					y = value;
				}
			}
		}
		public double Z
		{
			get 
			{
				return x;
			}
			set 
			{
				if (value < Int32.MinValue || value > Int32.MaxValue) {
					throw new Exception ("Out of range!");
				} else {
					z = value;
				}
			}
		}

		public Position ()
		{
		}
	}
}

