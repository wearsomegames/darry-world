﻿using System;
using OpenTK;

//Not to be confused with Position!!!
//World Position is used by superchunks to find THEIR position in the world.

namespace DarryWorld
{
	public class WorldPosition
	{
		private int x;
		private int z;
		private SuperChunkPosition sPos;
		public int X
		{
			get 
			{
				return x;
			}
			private set
			{
				
			}
		}
		public int Z
		{
			get 
			{
				return z;
			}
			private set
			{
				
			}
		}
		public SuperChunkPosition ToSuper
		{
			get 
			{
				return sPos;
			}
			set
			{
				sPos = value;	
			}
		}

		public WorldPosition (int x, int z, int y=0, SuperChunkPosition sPos=null)
		{
			this.x = x;
			this.z = z;
			this.sPos = sPos;
			if (sPos != null)
				sPos.ToWorld = this;
		}

		public WorldPosition (Vector3 v)
		{
			this.x = (int)Math.Floor(v.X/16);
			this.z = (int)Math.Floor(v.Z/16);
			this.sPos = new SuperChunkPosition(v, this);
		}

		public WorldPosition(WorldPosition wPos)
		{
			this.x = wPos.x;
			this.z = wPos.z;
			this.sPos = new SuperChunkPosition(wPos.ToSuper, wPos);
		}
	}
}

