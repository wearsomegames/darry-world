﻿using System;
using OpenTK;

//Used to find current Chunk position, used by Chunks.
//It can also be used to describe the position of a chunk or layerchunk.

namespace DarryWorld
{
	public class SuperChunkPosition
	{
		private int y;
		private WorldPosition wPos;
		private ChunkPosition cPos;
		public int Y
		{
			get 
			{
				return y;
			}
			private set 
			{
				if (value < 1 || value > 1825) {
					throw new Exception ("Out of range!");//TODO: proper exceptions
				} else {
					y = value;
				}
			}
		}
		public WorldPosition ToWorld
		{
			get 
			{
				return wPos;
			}
			set
			{
				wPos = value;
			}
		}
		public ChunkPosition ToChunk
		{
			get 
			{
				return cPos;
			}
			set
			{
				cPos = value;
			}
		}
		public SuperChunkPosition(int y, WorldPosition wPos)
		{
			this.y = y;
			cPos = new ChunkPosition(wPos.ToSuper.ToChunk, wPos.ToSuper);
			this.wPos = wPos;
		}
		public SuperChunkPosition(Vector3 pos, WorldPosition wPos)
		{
			this.wPos = wPos;
			this.y = (int)Math.Floor(pos.Y/16);
			this.ToChunk = new ChunkPosition(pos, this);
		}
		public SuperChunkPosition(SuperChunkPosition sPos, WorldPosition wPos)
		{
			this.wPos = wPos;
			y = sPos.Y;
			cPos = new ChunkPosition(sPos.ToChunk, this);
		}
	}
}

