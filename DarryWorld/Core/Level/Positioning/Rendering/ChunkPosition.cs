﻿using System;
using OpenTK;

//Used to find current position in Chunk, used by LayerChunks

namespace DarryWorld
{
	public class ChunkPosition
	{
		private int y;
		private SuperChunkPosition sPos;
		private LayerPosition lPos;
		public int Y 
		{
			get 
			{ 
				return y;
			}
			private set 
			{ 
				if (value < 1 || value > 16) {
					throw new Exception ("Out of range!");
				} else {
					y = value;
				}
			}
		}
		public SuperChunkPosition ToSuper
		{
			get 
			{
				return sPos;
			}
			set
			{
				sPos = value;
			}
		}
		public LayerPosition ToLayer
		{
			get 
			{
				return lPos;
			}
			set
			{
				lPos = value;
			}
		}

		public ChunkPosition (int pos)
		{
			y = pos;
		}

		public ChunkPosition(int pos, SuperChunkPosition sPos)
		{
			y = pos;
			this.sPos = sPos;
			this.ToLayer = new LayerPosition(sPos.ToChunk.ToLayer, sPos.ToChunk);
		}

		public ChunkPosition(Vector3 v, SuperChunkPosition sPos)
		{
			y = (int)Math.Floor(v.Y - 16 * sPos.Y);
			this.sPos = sPos;
			lPos = new LayerPosition(v, this);
		}

		public ChunkPosition (ChunkPosition cPos, SuperChunkPosition sPos)
		{
			y = cPos.y;
			this.sPos = sPos;
			lPos = new LayerPosition(cPos.ToLayer, this);
		}
	}
}