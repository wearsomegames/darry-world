﻿using System;
using OpenTK;

//Used to record position in current LayerChunk, this struct is used for tiles only.

namespace DarryWorld
{
	public class LayerPosition
	{
		private int x;
		private int z;
		private ChunkPosition cPos;
		public int X 
		{
			get 
			{
				return x;
			}
			private set 
			{
				if (value < 1 || value > 16) {
					throw new Exception ("Out of range!");//TODO: proper exceptions
				} else {
					x = value;
				}
			}
		}
		public int Z
		{
			get 
			{
				return z;
			}
			private set 
			{
				if (value < 1 || value > 16) 
				{
					throw new Exception ("Out of range!");//TODO: proper exceptions
				} 
				else 
				{
					z = value;
				}
			}
		}
		public ChunkPosition ToChunk
		{
			get 
			{
				return cPos;
			}
			set
			{
				cPos = value;
			}
		}

		public LayerPosition(int x, int z, ChunkPosition cPos)
		{
			this.x = x;
			this.z = z;
			if (cPos != null)
				cPos.ToLayer = this;
		}

		public LayerPosition(Vector3 v, ChunkPosition cPos)
		{
			this.x = (int)(v.X - cPos.ToSuper.ToWorld.X);
			this.z = (int)(v.Z - cPos.ToSuper.ToWorld.Z);
			cPos.ToLayer = this;
		}
		public LayerPosition(LayerPosition lPos, ChunkPosition cPos)
		{
			this.cPos = cPos;
			x = lPos.x;
			z = lPos.z;
		}
	}
}

