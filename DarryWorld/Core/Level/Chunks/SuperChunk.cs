﻿using System;
using System.Collections.Generic;

namespace DarryWorld
{
	public class SuperChunk
	{
		private World worldRef;
		private Chunk[] chunks = new Chunk[1825];//Height is so big so people can have 1 to 1 scale Mount Everests.
		private WorldPosition wPos = new WorldPosition(0,0);//default
		private bool isEmpty=true;//Should never be true, if it is, then mods are present
		//TODO: accessor for topChunk
		public WorldPosition Position
		{
			get 
			{
				return wPos;
			}
		}
		public bool IsEmpty
		{
			get 
			{ 
				return isEmpty; 
			}
		}

		public SuperChunk (World world, WorldPosition wPos)
		{
			worldRef = world;
			this.wPos = wPos;
		}

		#region Placement
		public void AllocateChunk(SuperChunkPosition pos, Chunk chunk)
		{
			if (chunks[pos.Y] == null)
				chunks[pos.Y] = chunk;
		}
		public void FillWholeSuper(Tile tile)
		{
			for(int i=0;i<chunks.Length;i++)
			{
				CheckGeneration(new SuperChunkPosition(i, wPos));
				chunks[i].FillWholeChunk(tile);
			}
		}
		public void FillSuperByRegion(Tile tile, WorldPosition pos1, WorldPosition pos2)
		{
			for (int i = pos1.ToSuper.Y; i < pos2.ToSuper.Y; i++) 
			{
				CheckGeneration(new SuperChunkPosition(i, wPos));
				chunks [i].FillChunkByRegion (tile, pos1.ToSuper.ToChunk, pos2.ToSuper.ToChunk);
			}
		}
		public void PlaceTile(Tile tile, WorldPosition pos)
		{
			CheckGeneration(pos.ToSuper);
			chunks [pos.ToSuper.Y].PlaceTile (tile, pos.ToSuper.ToChunk);
		}
		#endregion
		#region Removal
		public void EmptyWholeSuper()
		{
			for(int i=0;i<chunks.Length;i++)
			{
				CheckGeneration(new SuperChunkPosition(i, wPos));
				chunks[i].EmptyWholeChunk ();
			}
		}
		public void EmptySuperByRegion(WorldPosition pos1, WorldPosition pos2)
		{
			for (int i = pos1.ToSuper.Y; i < pos2.ToSuper.Y; i++) 
			{
				CheckGeneration(new SuperChunkPosition(i, wPos));
				chunks [i].EmptyChunkByRegion(pos1.ToSuper.ToChunk, pos2.ToSuper.ToChunk);
			}
		}
		public void RemoveTile(WorldPosition pos)
		{
			CheckGeneration(pos.ToSuper);
			chunks [pos.ToSuper.Y].RemoveTile (pos.ToSuper.ToChunk);
		}
		#endregion
		#region Utility
		public Chunk GetChunk (SuperChunkPosition pos)
		{
			if(pos.Y < 0 || pos.Y > chunks.Length - 1)
				return null;
			CheckGeneration(pos);
			return chunks[pos.Y];
		}
		private void IsPosition(params SuperChunkPosition[] positions)
		{
//			foreach(SuperChunkPosition pos in positions)
//			{
//				if (pos.ToChunk == default(ChunkPosition)) 
//				{
//					throw new Exception ("Is not a position!");
//				}
//			}
		}
		private bool CheckIntegrity()
		{
			foreach (Chunk chunk in chunks) 
			{
				if (!chunk.IsEmpty)
					return true;
			}
			return false;
		}
		private void CheckGeneration (SuperChunkPosition sPos)
		{
			if (chunks [sPos.Y] == null)
			{
				WorldPosition nPosC = new WorldPosition(wPos);
				nPosC.ToSuper = sPos; 
				worldRef.Generator.GenerateChunk (nPosC);
			}
		}
		#endregion
	}
}

