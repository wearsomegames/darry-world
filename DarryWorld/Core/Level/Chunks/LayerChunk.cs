﻿using System;
using System.Collections.Generic;
using OpenTK;

namespace DarryWorld
{
	public class LayerChunk
	{
		private World worldRef;
		private Tile[,] tiles = new Tile[16,16];
		private Chunk parentRef;
		private LayerChunk topTail;
		private LayerChunk accompaningUpperRef;
		private LayerChunk accompaningLowerRef;
		private ChunkPosition position;
		private bool isEmpty = true;
		private bool isHollow = false;
		public Chunk ParentRef 
		{
			get 
			{
				return parentRef;
			}
		}
		public LayerChunk AccompaningUpperRef
		{
			get 
			{ 
				if(accompaningUpperRef == null && position.Y < 15)
					accompaningUpperRef = parentRef.GetLayerChunk(new ChunkPosition(position.Y+1, position.ToSuper));
				return accompaningUpperRef; 
			}
		}
		public LayerChunk AccompaningLowerRef
		{
			get 
			{
				if(accompaningLowerRef == null && position.Y >= 0)
					accompaningLowerRef = parentRef.GetLayerChunk(new ChunkPosition(position.Y-1, position.ToSuper));
				return accompaningLowerRef; 
			}
		}
		public ChunkPosition Position
		{
			get 
			{
				return position;
			}
		}
		public bool IsEmpty
		{
			get 
			{ 
				return isEmpty; 
			}
		}
		public bool IsHollow
		{
			get 
			{
				return isHollow;
			}
		}
		public int WorldY 
		{
			get 
			{
				return (16 * parentRef.Position.Y) + position.Y;
			}
		}

		public LayerChunk (ChunkPosition pos, Chunk cRef, World worldRef)
		{
			position = pos;
			parentRef = cRef;
			this.worldRef = worldRef;
		}

		#region Placement
		public void FillWholeLayer (Tile tile)
		{
			for (int i = 0; i < tiles.Length/16; i++) 
			{
				for (int j = 0; j < tiles.Length/16; j++) 
				{
					tiles [i, j] = tile.CreateNew ();
				}
			}
		}
		public void FillLayerByRegion(Tile tile, LayerPosition pos1, LayerPosition pos2)
		{
			//assuming pos1.x and pos1.z is less than corresponding values in pos2.
			for(int i=pos1.Z;i<pos2.Z;i++)
			{
				for (int j = pos1.X; j < pos2.X; j++) 
				{
					tiles [i, j] = tile.CreateNew ();
				}
			}
		}
		public void PlaceTile(LayerPosition pos, Tile tile)
		{
			tiles [pos.Z, pos.X] = tile.CreateNew();
		}
		#endregion
		#region Removal
		public void EmptyWholeLayer()
		{
			for (int i = 0; i < tiles.Length; i++) 
			{
				for (int j = 0; j < tiles.Length; j++) 
				{
					tiles [i, j] = null;
				}
			}
			isEmpty = !CheckIntegrity ();
		}
		public void EmptyLayerByRegion(LayerPosition pos1, LayerPosition pos2)
		{
			//assuming pos1.x and pos1.z is less than corresponding values in pos2.
			for(int i=pos1.Z;i<pos2.Z;i++)
			{
				for (int j = pos1.X; j < pos2.X; j++) 
				{
					tiles [i, j] = null;
				}
			}
			isEmpty = !CheckIntegrity ();
		}
		public void RemoveTile(LayerPosition pos)
		{
			tiles [pos.Z, pos.X] = null;
			isEmpty = !CheckIntegrity ();
		}
		#endregion
		#region Utitlity
		private bool CheckIntegrity()
		{
			for(int i=0;i<tiles.Length;i++) 
			{
				for (int j=0;j<tiles.Length;i++)
				{
					if (tiles[i,j] != null) 
						return true;//is NOT empty
				}
			}
			return false; //is empty
		}
		public void ChangeTileScales (Vector2 scale)
		{
			for(int i=0;i<(tiles.Length)/16;i++) 
			{
				for (int j=0;j<(tiles.Length)/16;j++)
				{
					if (tiles[i,j] != null)
						tiles[i,j].Scale = scale;
				}
			}
		}
		public bool ExistsTile(LayerPosition pos)
		{
			return (tiles[pos.Z,pos.X] != null);
		}
		public bool ExistsTile (int x, int z)
		{
			return (tiles[z,x] != null);
		}
		public Tile GetTile (LayerPosition pos)
		{
			return tiles[pos.Z,pos.X];
		}
		public Tile GetTile (int z, int x)
		{
			return tiles[z,x];
		}
		public Tile[,] GetTiles()
		{
			return tiles;
		}
		public Vector3 TilePosition(int x, int z)
		{
			return new Vector3(x + parentRef.ParentRef.Position.X, position.Y + (parentRef.Position.Y * 16), z + parentRef.ParentRef.Position.Z);
		}
		#endregion
		#region PrivateUtility
		//Private Utility, unique to this class only
//		public LayerChunk[] GetTails() //get em sanic!
//		{
//			List<LayerChunk> headAndTails = new List<LayerChunk> ();
//			headAndTails.Add (this);
//		}
//		public LayerChunk[] GetTails(List<LayerChunk> buildingList)//alternate universe for sanic
//		{
//			if (!isEmpty)
//				buildingList.Add (this);
//			else if (topTail != null && !topTail.isEmpty) 
//			{
//				buildingList.Add (this);
//				//This whole layer is empty, but the one bellow isn't so it and the tail is added.
//				buildingList.Add (topTail);
//			}
//			else if (topTail == null) //Bottom of chain
//			{
//				return buildingList.ToArray ();
//			}
//		}
		#endregion
	}
}

