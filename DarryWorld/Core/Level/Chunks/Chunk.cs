﻿using System;

namespace DarryWorld
{
	public class Chunk
	{
		private World worldRef;
		private LayerChunk[] layerChunks = new LayerChunk[16];
		private SuperChunk parentRef;
		private Chunk accompaningUpperRef;
		private Chunk accompaningLowerRef;
		private SuperChunkPosition position;
		private bool isEmpty=true;
		private bool isHollow=false;

		public SuperChunk ParentRef 
		{
			get 
			{
				return parentRef;
			}
		}
		public Chunk AccompaningUpperRef
		{
			get 
			{
				if (accompaningLowerRef == null)
					accompaningUpperRef = parentRef.GetChunk(new SuperChunkPosition(position.Y+1, position.ToWorld));
				return accompaningUpperRef;
			}
		}
		public Chunk AccompaningLowerRef
		{
			get 
			{
				if (accompaningLowerRef == null)
					accompaningLowerRef = parentRef.GetChunk(new SuperChunkPosition(position.Y-1, position.ToWorld));
				return accompaningLowerRef;
			}
		}
		public SuperChunkPosition Position
		{
			get 
			{
				return position;
			}
		}
		public bool IsEmpty
		{
			get 
			{ 
				return isEmpty; 
			}
		}
		public bool IsHollow
		{
			get 
			{
				return isHollow;
			}
		}

		public Chunk (SuperChunkPosition pos, SuperChunk sRef, World worldRef)
		{
			position = pos;
			parentRef = sRef;
			this.worldRef = worldRef;
		}

		#region Placement
		public void AllocateLayer (ChunkPosition pos, LayerChunk layer)
		{
			if (layerChunks[pos.Y] == null)
				layerChunks[pos.Y] = layer;
			//else
				//throw new Exception("Layer chunk already allocated!");
		}
		public void FillWholeChunk (Tile tile)
		{
			for (int i = 0; i < layerChunks.Length; i++) 
			{
				CheckGeneration(new ChunkPosition(i, position));
				layerChunks[i].FillWholeLayer(tile);
			}
		}
		public void FillChunkByRegion(Tile tile, ChunkPosition pos1, ChunkPosition pos2)
		{
			IsPositions (pos1, pos2);
			for (int i = pos1.Y; i < pos2.Y; i++) 
			{
				CheckGeneration(new ChunkPosition(i, position));
				layerChunks [i].FillLayerByRegion (tile, pos1.ToLayer, pos2.ToLayer);
			}
		}
		public void PlaceTile(Tile tile, ChunkPosition pos)
		{
			IsPositions (pos);
			CheckGeneration(pos);
			layerChunks [pos.Y].PlaceTile (pos.ToLayer, tile);
		}
		#endregion
		#region Removal
		public void EmptyWholeChunk ()
		{
			for (int i = 0; i < layerChunks.Length; i++) 
			{
				CheckGeneration(new ChunkPosition(i, position));
				layerChunks[i].EmptyWholeLayer();
			}
		}
		public void EmptyChunkByRegion(ChunkPosition pos1, ChunkPosition pos2)
		{
			IsPositions (pos1,pos2);
			for (int i = pos1.Y; i < pos2.Y; i++) 
			{
				CheckGeneration(new ChunkPosition(i, position));
				layerChunks [i].EmptyLayerByRegion (pos1.ToLayer, pos2.ToLayer);
			}
		}
		public void RemoveTile(ChunkPosition pos)
		{
			CheckGeneration(pos);
			layerChunks [pos.Y].RemoveTile (pos.ToLayer);
		}
		#endregion
		#region Utility
		public LayerChunk GetLayerChunk (ChunkPosition pos)
		{
			if (pos.Y < 0 || pos.Y > layerChunks.Length-1)
				return null;
			CheckGeneration(pos);
			return layerChunks[pos.Y];
		}
		private static void IsPositions(params ChunkPosition[] positions)
		{
			foreach(ChunkPosition pos in positions)
			{
				if (pos.ToLayer == null) 
				{
					//throw new Exception ("Is not a position!");
				}
			}
		}
		private bool CheckIntegrity()
		{
			foreach(LayerChunk layer in layerChunks)
			{
				if (!layer.IsEmpty)
					return true;
			}
			return false;
		}
		private void CheckGeneration (ChunkPosition cPos)
		{
			if (layerChunks [cPos.Y] == null) 
			{
				WorldPosition wPos = position.ToWorld;
				wPos.ToSuper = cPos.ToSuper;
				wPos.ToSuper.ToChunk = cPos;
				worldRef.Generator.GenerateLayer (wPos);
			}
		}
		#endregion
	}
}

