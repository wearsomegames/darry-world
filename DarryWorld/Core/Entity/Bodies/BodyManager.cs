﻿using System;
using System.Collections.Generic;

namespace DarryWorld
{
	public class BodyManager
	{
		private static Dictionary<string, Body> bodies = new Dictionary<string, Body>();

		public static void AddBody(string tag, Body body)
		{
			bodies.Add(tag, body);
		}

		public static Body GetBody (string key)
		{
			return bodies[key];
		}
	}
}

