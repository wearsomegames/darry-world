﻿using System;
using OpenTK;

namespace DarryWorld
{
	public class Entity : IEntityAI
	{
		protected internal int id;
		protected internal int health;
		protected internal int damage;
		protected internal double speed;
		protected internal Body body;
		protected internal HitBox3D hitBox;

		public int ID{get { return id; }}
		public int Health{get { return health; }}
		public int Damage{get { return damage; }}

		public Entity ()
		{
			//TODO: Implement this thing
		}

		public void InvokeAI()
		{
			if (health < 1)
			{
				//destroy
			}
		}

		public void AttackThis(int damage)
		{
			health -= damage;
		}

		public bool IsCollide(Vector3 pos)
		{
			return hitBox.WithinHitBox(pos);
		}
	}
}

