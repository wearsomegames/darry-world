﻿using System;
using System.Collections.Generic;
using OpenTK;

namespace DarryWorld
{
	public enum HitBoxDirection
	{
		Above,//y
		Below,//y
		Inside//y=py || z=pz || x=px
	}
	public class HitBox3D
	{
		private Vector3[] verticies;

		public HitBox3D (params Vector3[] verticies)
		{
			if (verticies.Length == 0) 
			{
				//throw
			}
			this.verticies = verticies;
		}

		public bool WithinHitBox(Vector3 pos)
		{
			List<HitBoxDirection[]> respectives = new List<HitBoxDirection[]>();
			for(int i=0;i<verticies.Length;i++)
			{
				respectives.Add(RespectTo(pos, verticies[i]));
			}
			for(int i=0;i<respectives.Count;i++)
			{
				for(int j=0;j<respectives.Count;j++)
				{
					if (respectives[i][0]==respectives[j][0] &&
						respectives[i][1]==respectives[j][1] &&
						respectives[i][2]==respectives[j][2])
						return false;
				}
			}
			return true;
		}

		private HitBoxDirection[] RespectTo(Vector3 pos, Vector3 compare)
		{
			HitBoxDirection[] respectBox = new HitBoxDirection[3];
			//y
			if (pos.Y < compare.Y)
				respectBox[0]=HitBoxDirection.Below;
			else if (pos.Y > compare.Y)
				respectBox[0]=HitBoxDirection.Above;
			else
				respectBox[0]=HitBoxDirection.Inside;
			//z
			if (pos.X < compare.X)
				respectBox[1]=HitBoxDirection.Below;
			else if (pos.X > compare.X)
				respectBox[1]=HitBoxDirection.Above;
			else
				respectBox[1]=HitBoxDirection.Inside;
			//x
			if (pos.Z < compare.Z)
				respectBox[2]=HitBoxDirection.Below;
			else if (pos.Z > compare.Z)
				respectBox[2]=HitBoxDirection.Above;
			else
				respectBox[2]=HitBoxDirection.Inside;

			return respectBox;
		}
	}
}

