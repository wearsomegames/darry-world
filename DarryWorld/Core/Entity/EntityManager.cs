﻿using System;
using System.Collections.Generic;

namespace DarryWorld
{
	public static class EntityManager
	{
		private static Dictionary<int, Entity> entities = new Dictionary<int, Entity>(); //By ID
		//TODO: Implement
		//Entity Manager is not only the manager for entities, but also
		//the sprite batch from which entity textures are recived.
		public static void AddEntity(Entity entity)
		{
			entities.Add (entity.ID, entity);
		}

		public static Entity GetEntityByID(int ID)
		{
			return entities [ID];
		}

		//Sprite Batch :
	}
}

