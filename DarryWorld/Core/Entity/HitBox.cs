﻿using System;
using System.Collections.Generic;
using OpenTK;

namespace DarryWorld
{
	public class HitBox
	{
		//2D hitbox but 3D vectors? WHAT IS THIS MADDNESS?!
		//I will tell you!
		//Basically HitBox ignores Y space, while HitBox3D doesn't.

		//NOTE! HitBox and HitBox3D only works for verticies forming squares or cubes respectively.
		//Use HitPoly for other shapes.
		private Vector3[] verticies;

		public HitBox (params Vector3[] verticies)
		{
			this.verticies = verticies;
		}

		public bool WithinBox (Vector3 pos)
		{
			List<HitBoxDirection[]> respectives = new List<HitBoxDirection[]> ();
			for (int i = 0; i < verticies.Length; i++) 
			{
				for (int j = 0; j < verticies.Length; j++) 
				{
					if (respectives[i][0] == respectives[j][0] &&
						respectives[i][1] == respectives[j][1])
						return false;
				}
			}
			return true;
		}

		private HitBoxDirection[] RespectTo(Vector3 pos, Vector3 respect)
		{
			//Ignore y space check
			HitBoxDirection[] respective = new HitBoxDirection[2];
			//x
			if (pos.X < respect.X)
				respective[0] = HitBoxDirection.Below;
			if (pos.X > respect.X)
				respective[0] = HitBoxDirection.Above;
			if (pos.X == respect.X)
				respective[0] = HitBoxDirection.Inside;
			//z
			if (pos.Z < respect.Z)
				respective[1] = HitBoxDirection.Below;
			if (pos.Z > respect.Z)
				respective[1] = HitBoxDirection.Above;
			if (pos.Z == respect.Z)
				respective[1] = HitBoxDirection.Inside;
			return respective;
		}
	}
}

