﻿using System;
using System.Collections.Generic;
using OpenTK.Input;
using OpenTK;

namespace DarryWorld
{
	public static class GameInput
	{
		private static List<Key> keysDown;
		private static List<Key> keysDownLast;
		private static List<MouseButton> buttonsDown;
		private static List<MouseButton> buttonsDownLast;

		public static void Initalize(GameWindow game)
		{
			Console.WriteLine ("Instantiating GameInput");
			keysDown = new List<Key> ();
			keysDownLast = new List<Key> ();
			buttonsDown = new List<MouseButton> ();
			buttonsDownLast = new List<MouseButton> ();

			game.KeyDown += Game_KeyDown;
			game.KeyUp += Game_KeyUp;
			game.MouseDown += Game_MouseDown;
			game.MouseUp += Game_MouseUp;
			Console.WriteLine ("GameInput Instantiated!");
		}

		static void Game_MouseUp (object sender, OpenTK.Input.MouseButtonEventArgs e)
		{
			while (buttonsDown.Contains (e.Button)) 
			{
				buttonsDown.Remove (e.Button);
			}
		}

		static void Game_MouseDown (object sender, OpenTK.Input.MouseButtonEventArgs e)
		{
			buttonsDown.Add (e.Button);
		}

		static void Game_KeyUp (object sender, OpenTK.Input.KeyboardKeyEventArgs e)
		{
			while (keysDown.Contains (e.Key)) 
			{
				keysDown.Remove (e.Key);
			}
		}

		static void Game_KeyDown (object sender, OpenTK.Input.KeyboardKeyEventArgs e)
		{
			keysDown.Add (e.Key);
		}

		public static void Update()
		{
			keysDownLast = new List<Key> (keysDown);
			buttonsDownLast = new List<MouseButton> (buttonsDown);
		}

		public static bool KeyPress(Key key)
		{
			return (keysDown.Contains (key) && !keysDownLast.Contains (key));
		}

		public static bool KeyRelease(Key key)
		{
			return (!keysDown.Contains (key) && keysDownLast.Contains (key));
		}

		public static bool KeyDown(Key key)
		{
			return (keysDown.Contains (key));
		}

		public static bool MousePress(MouseButton button)
		{
			return (buttonsDown.Contains (button) && !buttonsDownLast.Contains (button));
		}

		public static bool MouseRelease(MouseButton button)
		{
			return (!buttonsDown.Contains (button) && buttonsDownLast.Contains (button));
		}

		public static bool MouseDown(MouseButton button)
		{
			return (buttonsDown.Contains (button));
		}
	}
}

