﻿using System;
using OpenTK;

namespace DarryWorld
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			DisplayDevice device = DisplayDevice.GetDisplay(DisplayIndex.Primary);
			Game window = new Game (device.Bounds.Width,device.Height);
			Console.WriteLine ("Darry World");
			long memory = GC.GetTotalMemory(true);
			Console.WriteLine("==Allocated memory== \ngb:{0}\nmb:{1}\nkb:{2}\nbytes:{3}", memory/1073741824, memory/(1048576), memory/1024, memory);
			window.Run ();
		}
	}
}
